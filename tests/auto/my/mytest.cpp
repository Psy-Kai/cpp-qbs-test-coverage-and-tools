#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <mylib/my.h>

namespace my {
namespace mock {

class Use : public UseInterface {
public:
    MOCK_METHOD(int, useMe, (), (noexcept, override));
};

} // namespace mock

struct my_Foobar_doSomething : public testing::Test {
    testing::StrictMock<mock::Use> use;
    Foobar foobar{use};
    const int value = std::abs(std::rand());
};

TEST_F(my_Foobar_doSomething, multGreaterThan0)
{
    const auto mult = std::abs(std::rand());
    EXPECT_CALL(use, useMe).WillOnce(testing::Return(value));
    ASSERT_EQ(foobar.doSomething(mult), value * mult);
}

TEST_F(my_Foobar_doSomething, mult0)
{
    const auto mult = 0;
    ASSERT_EQ(foobar.doSomething(mult), 0);
}

} // namespace my

int main(int argc, char **argv)
{
    std::srand(std::time(nullptr));
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
