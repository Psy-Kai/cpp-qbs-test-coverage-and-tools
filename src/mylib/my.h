namespace my {

class UseInterface {
public:
    virtual ~UseInterface() = default;
    virtual int useMe() noexcept = 0;
};

class Interface {
public:
    virtual ~Interface() = default;
    virtual int doSomething(int) noexcept = 0;
};

class Foobar final : public Interface {
public:
    explicit Foobar(UseInterface &use) noexcept;
    int doSomething(int mult) noexcept override;

private:
    UseInterface &m_use;
};

} // namespace my
